#!/usr/bin/env python3
"""
This file extracts GPS position (latitude and longitude) from device trace logger file.
"""
import click
import simplekml


@click.command()
@click.option('--logfile', '-f', type=click.STRING, help='Log file')
class GpsExtractor:
    def __init__(self, logfile: None):
        self.logfile = logfile
        self.route_start_time = ""
        self.route_stop_time = ""
        coords = self.load_coords(logfile)
        # self.create_kml(coords)

    def load_coords(self, file):
        index_list = []
        coords = []
        line_num = 1

        with open(file, 'r') as in_file:
            for line in in_file:
                line_split = line.split(" ")
                timestamp = line_split[1]
                if line_num == 1:
                    self.route_start_time = timestamp
                self.route_stop_time = timestamp
                string_list = [' lon: ', ', lat:', ', h:']
                for item in string_list:
                    index_list.append(line.find(item))
                longitude_info = line[index_list[0] + len(string_list[0]):index_list[1]]
                latitude_info = line[index_list[1] + len(string_list[1]):index_list[2]]

                if(len(latitude_info) != 0) and (len(longitude_info) != 0):
                    print("lat: {} lon:{}".format(latitude_info, longitude_info))
                    # gps_position = (float(longitude_info), float(latitude_info))
                    # coords.append(gps_position)
        return coords

    # def create_kml(self, coords) -> None:
    #     kml = simplekml.Kml()
    #     lin = kml.newlinestring(name='drive_path', description="A gps logging pathway", coords=coords)
    #     lin.style.linestyle.color = simplekml.Color.blue  # Update line color
    #     lin.style.linestyle.width = 20  # Update line width pixels
    #     lin.timespan.begin = self.route_start_time
    #     lin.timespan.end = self.route_stop_time
    #     kml.save("pathway.kml")


if __name__ == "__main__":
    GpsExtractor()
